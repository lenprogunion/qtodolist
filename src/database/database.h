#ifndef DATABASE_H
#define DATABASE_H

#define DATABASE_HOSTNAME   "sqlite"
#define DATABASE_NAME       "todo.db"

#define TABLE_NAME          "todos"
#define TABLE_COL_TEXT      "text"
#define TABLE_COL_COMPLITED "comlpited"

#include <QObject>
#include <QSql>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlDatabase>
#include <QDebug>

class Database : public QObject
{
    Q_OBJECT
public:
    explicit Database(QObject *parent = nullptr);
    ~Database();
    bool connectDataBase();

signals:

private:
    QSqlError getLastError();
    QSqlDatabase db;
    void closeDataBase();
    bool createTable();

public slots:
    int inserIntoTable(const QString &text);
    bool removeRecord(const int id);
    bool editRecord(const int id, const QString &text);

    QSqlQuery getAll();
};

#endif // DATABASE_H
