#include "database.h"


Database::Database(QObject *parent)
    : QObject{parent}
{

}

Database::~Database()
{

}

bool Database::connectDataBase(){
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(DATABASE_NAME);
    if(!db.open()){
        return false;
    }
    if(!createTable()){
        return false;
    }

    return true;
}

void Database::closeDataBase()
{
    db.close();
}

bool Database::createTable()
{
    QSqlQuery query;
    if(!query.exec( "CREATE TABLE IF NOT EXISTS " TABLE_NAME " ("
                            "id INTEGER PRIMARY KEY AUTOINCREMENT, "
                            TABLE_COL_TEXT " VARCHAR(200)    NOT NULL, "
                            TABLE_COL_COMPLITED " NUMERIC    NOT NULL DEFAULT FALSE"
                        " )"
                    )){
        qDebug() << "DataBase: error of create " << TABLE_NAME;
        qDebug() << query.lastError().text();
        return false;
    }
    return true;
}



int Database::inserIntoTable(const QString &text)
{
    QSqlQuery query;
    query.prepare("INSERT INTO " TABLE_NAME " ( " TABLE_COL_TEXT " ) "
                                        "VALUES (:text)");
    query.bindValue(":text",  text);

    if(!query.exec()){
        qDebug() << "error insert into " << TABLE_NAME;
        qDebug() << query.lastError().text();
        return 0;
    }
    return query.lastInsertId().toInt();
}

bool Database::removeRecord(const int id)
{
    QSqlQuery query;

    query.prepare("DELETE FROM " TABLE_NAME " WHERE id= :ID ;");
    query.bindValue(":ID", id);

    if(!query.exec()){
        qDebug() << "error delete row " << TABLE_NAME;
        qDebug() << query.lastError().text();
        return false;
    }
    return true;
}

bool Database::editRecord(const int id, const QString &text){
    QSqlQuery query;

    query.prepare("UPDATE " TABLE_NAME " SET text=:TEXT WHERE id= :ID ;");
    query.bindValue(":TEXT", text);
    query.bindValue(":ID", id);

    if(!query.exec()){
        qDebug() << "error update row " << TABLE_NAME;
        qDebug() << query.lastError().text();
        return false;
    }
    return true;
}

QSqlQuery Database::getAll(){
    QString text_query = "SELECT id, text FROM " TABLE_NAME;
    QSqlQuery query;
    query.exec(text_query);
    return query;
}
