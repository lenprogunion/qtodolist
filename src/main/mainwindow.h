#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "../database/database.h"
#include "../task/task.h"

#include <QMainWindow>
#include <QInputDialog>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void addTask();


public slots:
    void removeTask(Task *task);
private slots:
    void on_addTaskButton_clicked();

private:
    void createTask(QString &name, int id);
    int addTaskInDatabase(QString &name);
    Database *db;
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
