#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{

    ui->setupUi(this);
    QSqlQuery query;
    query = db->getAll();
    while(query.next()){
        QString name = query.value("text").toString();
        int id = query.value("id").toInt();
        createTask(name, id);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::addTask()
{
    bool ok;
    QString name = QInputDialog::getText(this,
                                         tr("Добавить задачу"),
                                         tr("Задача"),
                                         QLineEdit::Normal,
                                         tr("Текст задачи"), &ok);
    if (ok && !name.isEmpty()) {

        qDebug() << "Adding new task";
        int id = addTaskInDatabase(name);
        createTask(name, id);
    }
}

void MainWindow::createTask(QString &name, int id){
    Task *task = new Task(name, id);
    connect(task, &Task::removed, this, &MainWindow::removeTask);
    ui->tasksLayout->addWidget(task);
}

int MainWindow::addTaskInDatabase(QString &name){

    return db->inserIntoTable(name);
}

void MainWindow::removeTask(Task *task)
{
    int id = task->findChild<QObject *>("removeButton")->property("id").toInt();
    db->removeRecord(id);
    ui->tasksLayout->removeWidget(task);
    delete task;
}

void MainWindow::on_addTaskButton_clicked()
{
    addTask();
}

