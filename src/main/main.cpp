#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Database db;
    if(!db.connectDataBase()){
        return 1;
    }

    MainWindow w;
    w.show();
    return a.exec();
}


