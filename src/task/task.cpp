#include "task.h"

#include "ui_task.h"

Task::Task(const QString &name, const int id, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Task)
{
    ui->setupUi(this);
    setName(name);
    connect(ui->checkbox, &QCheckBox::toggled, this, &Task::checked);
    setPropertyButton(id);
}

Task::~Task()
{
    delete ui;
}

void Task::setPropertyButton(const int id){
    ui->removeButton->setProperty("id", id);
    ui->editButton->setProperty("id", id);
}

void Task::setName(const QString &name)
{
    ui->checkbox->setText(name);
}

QString Task::name() const
{
    return ui->checkbox->text();
}

bool Task::isCompleted() const
{
    return ui->checkbox->isChecked();
}

void Task::edit()
{
    bool ok;
    QString value = QInputDialog::getText(this, tr("Редактировать задачу"),
                                          tr("Задача"),
                                          QLineEdit::Normal,
                                          this->name(), &ok);
    if (ok && !value.isEmpty()) {
        setName(value);
        int id = this->property("id").toInt();
        db->editRecord(id, value);
    }
}

void Task::checked(bool checked)
{
    QFont font(ui->checkbox->font());
    font.setStrikeOut(checked);
    ui->checkbox->setFont(font);
}

void Task::on_editButton_clicked()
{
    edit();
}

void Task::on_removeButton_clicked()
{
    emit removed(this);
}




