#ifndef TASK_H
#define TASK_H

#include "../database/database.h"

#include <QWidget>
#include <QInputDialog>
#include <QLineEdit>
namespace Ui {
class Task;
}

class Task : public QWidget
{
    Q_OBJECT

public:
    explicit Task(const QString &name, const int id, QWidget *parent = nullptr);
    ~Task();
    void setName(const QString &name);
    void setPropertyButton(const int id);
    QString name() const;
    bool isCompleted() const;
    void edit();

signals:
    void removed(Task *task);

private slots:
    void checked(bool checked);
    void on_editButton_clicked();
    void on_removeButton_clicked();

private:
    Database *db;
    Ui::Task *ui;
};

#endif // TASK_H
